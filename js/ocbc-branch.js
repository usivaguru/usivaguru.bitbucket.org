$(document).ready(function() {

  var map;
  var src = "http://www.ocbc.com.my/assets/data/locate-us/ALL_BRANCH_ATM.KML";
  var markers = [];
  var kmlLayer;
  var bounds;
  var config = {
    apiSrc: "https://maps.googleapis.com/maps/api/js?v=3.exp",
    center: {
      lat: 4.140634,
      lng: 109.6181485
    }
  };

  initialize();

  function initialize() {
    loadMapsApi(initMap);
  }

  function loadMapsApi(cb) {
    if (window.google) {
      cb();
      return;
    }
    var script = document.createElement("script");
    script.src = config.apiSrc + "&callback=rpOCBCLoadGMaps";
    document.body.appendChild(script);

    window.rpOCBCLoadGMaps = cb;
  }

  function initMap() {
    var lat = config.center.lat;
    var lng = config.center.lng;
    var mapOptions = {
      center: new google.maps.LatLng(lat, lng),
      zoom: 7,
      maptype: 'ROADMAP',
      scrollwheel: false,
      draggable: false
    };
    map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);
    loadKmlLayer(src, map);
    bindMapClick();
    getCoordinates(src);
  }

  function getCoordinates(src) {
    $.ajax({
      type: "GET",
      url: "http://usivaguru.bitbucket.org/js/branch.kml",
      contentType: "xml",
      xhrFields: {
      //xhrFields
      },
      headers: {
      //custom headers
      },

      success: function(result,status,xhr) {
       $(result).find("Placemark").each(function(index, value){
          //get coordinates and place name
          var coords = $(this).find("coordinates").text();
          var name = $(this).find("name").text();
          var icon = $(this).find("href").text();
          var description = $(this).find("description").text();
          var c = coords.split(",");
          var marker = new google.maps.Marker({
            map: null,
            position: new google.maps.LatLng(c[1],c[0]),
            name: name.replace(/(<([^>]+)>)/ig,""),
            description: description
          });
          markers.push(marker);
        });
        findNearestBranch();
      },

      error: function(xhr,status,error) {
        console.log("xhr: " + xhr);
        console.log("status: " + status);
        console.log("error: " + error);
      }
    });
  }

  function bindMapClick() {
    google.maps.event.addListener(map, "dblclick", function () {
      map.setOptions({scrollwheel: true, draggable: true});
    });
  }

  function loadKmlLayer(src, map) {
    infowindow = new google.maps.InfoWindow();
    kmlLayer = new google.maps.KmlLayer({
      url: src,
      suppressInfoWindows: true,
      preserveViewport: false,
      map: map
    });

    google.maps.event.addListenerOnce(map, "idle", function(event) {
      bounds = kmlLayer.getDefaultViewport();
      map.fitBounds(bounds);
    });

    google.maps.event.addListener(kmlLayer, "click", function(event) {
      var content = event.featureData.infoWindowHtml;
      var branchName = event.featureData.name;
      var branchAddress = event.featureData.description;
      var fullAddress = branchName + branchAddress;
      var latLng = event.latLng;
      infowindow.close();
      infowindow.setOptions({
        content: content,
        position: event.latLng
      });
      infowindow.open(map);
      map.panTo(event.latLng);
      updateAddressField(branchName);
    });
  }

  function updateAddressField(address) {
    var $addressField = $("div#fullBranchAddress address#branchAddress");
    $addressField.html(address);
    $addressField.find("p:eq(1)").remove();
    $addressField.find("p > *").unwrap();
  }

  function findNearestBranch() {
    var $selector = $("a.nearestBranch");
    var $geoLocFallback = $("div#geolocation-fallback");
    var $geoLocFallbackError = $("div#geolocation-fallback p.geolocation-errormsg");
    $selector.on("click", function(e) {
      e.preventDefault();
      if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(geo_success, geo_error, geo_options);
      } else {
        $geoLocFallbackError.text("Sorry, we were not able to get your current position. Please check if your browser supports geolocation");
        $geoLocFallback.slideDown().removeClass("hide");
      }
    });
  }

  function geo_success(position) {
    var pos = new google.maps.LatLng(position.coords.latitude,
                                     position.coords.longitude);
    var $geoLocFallback = $("div#geolocation-fallback");
    $geoLocFallback.addClass("hide");
    find_closest_marker(position);
  }

  function geo_error() {
    var $geoLocFallback = $("div#geolocation-fallback");
    var $geoLocFallbackError = $("div#geolocation-fallback p.geolocation-errormsg");
    $geoLocFallbackError.text("Sorry, we were not able to get your current position. Please check if geolocation is enabled.")
    $geoLocFallback.slideDown().removeClass("hide");
  }

  var geo_options = {
    enableHighAccuracy: true,
    maximumAge        : 30000,
    timeout           : 27000
  };

  function rad(x) {return x*Math.PI/180;}
  function find_closest_marker( position ) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    var R = 6371; // radius of earth in km
    var distances = [];
    var closest = -1;
    var closestMarker;
    var latLng;
    for( i = 0; i < markers.length; i++ ) {
      var mlat = markers[i].position.lat();
      var mlng = markers[i].position.lng();
      var dLat  = rad(mlat - lat);
      var dLong = rad(mlng - lng);
      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(rad(lat)) * Math.cos(rad(lat)) * Math.sin(dLong/2) * Math.sin(dLong/2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      var d = R * c;
      distances[i] = d;
      if ( closest == -1 || d < distances[closest] ) {
          closest = i;
      }
    }

    infowindow = new google.maps.InfoWindow();
    closestMarker = markers[closest];
    infowindow.close();
    contentString = getNearestBranchWindowContent(closestMarker);
    map.setZoom(13);
    map.panTo(closestMarker.getPosition());
    infowindow.setContent(contentString);
    infowindow.open(map, closestMarker);
    updateAddressField(closestMarker.name);
  }

  function getNearestBranchWindowContent(closestMarker) {
    var title = "<h4>Nearest OCBC Branch</h4>";
    var branchName = closestMarker.name;
    var branchDescription = closestMarker.description;
    var content = title + branchName + branchDescription;
    return content;
  }
});
